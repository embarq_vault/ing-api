'use strict';

module.exports = function(Invoice) {
  // The code below disables same remoting methods for the Invoice model
  // according to ones configured in server/config.json
  // This also will remove restricted model methods from explorer

  const globalModelConfig = require('../../server/config.json');

  // Disable methods only for Invoice->InvoiceItem relation
  const relationName = 'items';
  for (let [methodName, isMethodEnabled] of Object.entries(globalModelConfig.remoting.sharedMethods)) {
    if (!isMethodEnabled) {
      Invoice.disableRemoteMethodByName(`__${ methodName }__${ relationName }`);
    }
  }
};
