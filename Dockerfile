FROM node:12.16-slim

ARG mongodb_uri

ENV MONGODB_URI=$mongodb_uri
ENV NODE_ENV=production

WORKDIR /home/api-app

COPY package.json /home/api-app/
COPY package-lock.json /home/api-app/

RUN npm ci

COPY . /home/api-app

EXPOSE 3000 3200
CMD [ "npm", "start" ]
